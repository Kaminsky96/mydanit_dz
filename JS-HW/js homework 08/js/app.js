const input = document.getElementById("number");
const divPrice = document.getElementById("price");


input.addEventListener("focus", () => input.classList.add("focused"), true);
input.addEventListener("blur", () => input.classList.remove("focused"), true);

const span = document.createElement("span");
const btn = document.createElement('button');
const p = document.createElement('p');

input.onblur = function () {
  divPrice.classList.add("price");
  divPrice.append(span);
  span.textContent = `Текущая цена:${input.value}$`
  span.append(btn);
  btn.classList.add('close-btn')
  btn.textContent = 'x';
  btn.addEventListener('click', function() {
      divPrice.remove();
      document.getElementById("number").value = "";
  });
  if (input.value < 1) {
      input.classList.add('bad-price'); 
      divPrice.remove();
      document.body.append(p);
      p.textContent = 'Please enter correct price';
}
};




