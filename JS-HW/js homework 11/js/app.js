const btn = document.querySelectorAll(".btn");

document.addEventListener("keydown", (evt) => {
  btn.forEach((element) => {
    if (evt.key.toLowerCase() === element.id.toLowerCase()) {
      element.classList.add("btn-active");
    } else {
      element.classList.remove("btn-active");
    }
  });
});
