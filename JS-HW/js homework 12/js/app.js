let imgContent = document.querySelectorAll('.image-none');
let currentImg = 0;
let slider = setInterval(imgSlider, 5000);

function imgSlider() {
    imgContent[currentImg].classList.remove('current-img');
    currentImg = (currentImg + 1) % imgContent.length;
    imgContent[currentImg].classList.add('current-img');
}

const btnContainer = document.createElement('div');
btnContainer.classList.add('btn-wrapper');
document.querySelector('script').before(btnContainer);
const stopTimeOut = document.createElement('button');
stopTimeOut.classList.add('btn');
stopTimeOut.innerText = 'Прекратить показ';
btnContainer.appendChild(stopTimeOut);
stopTimeOut.addEventListener('click', ()=> {
    clearInterval(slider);
});
const resumeTimeout = document.createElement('button');
resumeTimeout.classList.add('btn');
resumeTimeout.innerText = 'Возобновить показ';
btnContainer.appendChild(resumeTimeout);
document.querySelector('script').before(btnContainer);
resumeTimeout.addEventListener('click',()=> {
    slider = setInterval(imgSlider, 5000)
});

imgSlider();


