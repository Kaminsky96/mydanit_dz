const link = document.createElement('link');
link.rel = 'stylesheet';
link.href = './css/theme.css';

document.querySelector('.main-button').addEventListener('click', ()=>{
    if(localStorage.getItem('theme')){
        localStorage.removeItem('theme');
        link.remove();
    } else {
        localStorage.setItem('theme', 'black');
        document.head.append(link);
    }
});

document.addEventListener('DOMContentLoaded', ()=>{
    if(localStorage.getItem('theme')){
        document.head.append(link);
    }
})