const eyePassword = document.getElementById("eye-password");
const eyePasswordConfirm = document.getElementById("eye-password-confirm");
const password = document.getElementById("password");
const confirmPassword = document.getElementById("confirm-password");
const submit = document.getElementById("submit");
const p = document.createElement("p");
const inputSecond = document.getElementById("last-input");

eyePassword.addEventListener("click", (evt) => {
  if (password.type === "password") {
    eyePassword.classList.remove("fa-eye");
    eyePassword.classList.add("fa-eye-slash");
    password.setAttribute("type", "text");
  } else {
    password.type = "password";
    eyePassword.classList.remove("fa-eye-slash");
    eyePassword.classList.add("fa-eye");
  }
});

eyePasswordConfirm.addEventListener("click", (evt) => {
  if (confirmPassword.type === "password") {
    eyePasswordConfirm.classList.remove("fa-eye");
    eyePasswordConfirm.classList.add("fa-eye-slash");
    confirmPassword.setAttribute("type", "text");
  } else {
    confirmPassword.type = "password";
    eyePasswordConfirm.classList.remove("fa-eye-slash");
    eyePasswordConfirm.classList.add("fa-eye");
  }
});
submit.addEventListener("click", (evt) => {
  event.preventDefault();
  if (
    password.value !== confirmPassword.value ||
    password.value === "" &&
    confirmPassword.value === ""
  ) {
    inputSecond.append(p);
    p.textContent = "Нужно ввести одинаковые значения";
    p.style.color = 'red';
  }
  if (password.value === confirmPassword.value && password.value !== "" &&
  confirmPassword.value !== "" ) {
    p.remove();
    alert("You are welcome!");
    
  }
});
