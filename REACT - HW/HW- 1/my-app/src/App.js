import React, { Component } from "react";
import "./App.scss";
import Header from "./containers/Header.jsx";
import AppRouter from "./components/AppRouter/AppRouter.jsx";

// class App extends React.Component {

//   render() {

//     return (

//       <div className="App">
//         <Header/>
//         <AppRouter/>
//       </div>
//     );
//   }
// }
// export default App;
const App = (props) => {
  return (
    <div className="App">
      <Header />
      <AppRouter />
    </div>
  );
};

export default App;
