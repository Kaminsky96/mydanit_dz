import React, { Component } from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import Basket from "../../pages/Basket";
import Favourites from "../../pages/Favourites.jsx";
import Home from "../../pages/Home.jsx";

// class AppRouter extends Component {
//     render(){
//         return(
//             <Switch>
//             <Redirect exact from='/' to='/Home'/>
//             <Route exact path="/Home" component={Home}/>
//             <Route  exact path="/Bascet" component={Basket}/>

//             <Route exact path="/Favourites" component={Favourites}/>

//   </Switch>
//         )
//     }

// }

// export default AppRouter;

const AppRouter = (props) => {
  return (
    <Switch>
      <Redirect exact from="/" to="/Home" />
      <Route exact path="/Home" component={Home} />
      <Route exact path="/Bascet" component={Basket} />

      <Route exact path="/Favourites" component={Favourites} />
    </Switch>
  );
};

export default AppRouter;
