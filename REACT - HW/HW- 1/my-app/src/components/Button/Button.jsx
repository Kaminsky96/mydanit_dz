import React from "react";
import "../../App.scss";

// class Button extends Component {
// constructor(props){
//     super(props)

// }
// render() {

//     return (
//         <>
//       <button className = "btn" style={{background:this.props.background}} onClick={this.props.action}>
//           {this.props.text}
//       </button>
//       </>
//     );
//   }
// }

// export default Button;

const Button = (props) => {
  return (
    <>
      <button
        className={props.className}
        style={{ background: props.background, display: props.display }}
        onClick={props.action}
      >
        {props.text}
      </button>
    </>
  );
};

export default Button;
