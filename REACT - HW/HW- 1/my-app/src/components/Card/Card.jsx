import React, { useEffect, useState } from "react";
import "../../App.scss";
import Button from "../Button/Button.jsx";
import Modal from "../Modal/Modal.jsx";
import { BsFillStarFill } from "react-icons/bs";
import getCars from "../../api/getCars";

// class Card extends Component{
//     state = {
//         starColor: 'white',
//         showBasket: false,
//         isShown:true
//     }
//     openBasket = () => {
//         this.setState({ showBasket: true });

//     };
//     closeBasket = () => {
//         this.setState({ showBasket: false });

//     };
//     handleAddtoCart = ()=> {
//         this.openBasket();
//         this.props.getId();
//     }

//     drawStar = () => {
//         const {starColor} = this.state
//         const {addToFavourites} = this.props

//         if(starColor === 'white') {
//             this.setState({starColor: 'yellow'})
//         } else if(starColor === 'yellow') {
//             this.setState({starColor: 'white'})
//         }

//         addToFavourites()
//     }

//     render(){
//         const {name, color, url, price, addToPurchased,getId} = this.props;
//         const modalBasket = (
//             <Modal text={<p>Confirm your purchase :<br/>
//             <img src={url} alt="" width="200" height="150"/><br/> {name}<br/>color: {color}. Price: {price}$</p>} header={"Confirmation"} action={this.closeBasket}
//             actions={
//               <>
//               <Button text={'ok'} background={"orange"} action={()=>
//               { addToPurchased()
//               }}
//           />
//               <Button text={'cancel'} action={this.closeBasket} background={"orange"} />
//               </>
//             }/>
//           );
//         return(
//             <>
//             {this.state.showBasket && modalBasket}
//             <div>
//             <div className="card-container">
//                    <img src={url} alt="" width="200" height="150"/>
//                    <p>{name}<BsFillStarFill color={this.state.starColor} onClick={this.drawStar}/></p>
//                    <p>Color:{color}</p>
//                    <p className="price">Price:{price}$</p>
//                  {this.state.isShown && <Button text={"Add to card"} background={"orange"} action={this.handleAddtoCart}/>}
//                  </div>
//             </div>
//                  </>
//         )
//     }
// }

// export default Card;

const Card = (props) => {
  const [starColor, setStarColor] = useState("white");
  const [showBasket, setShowBasket] = useState(false);
  useEffect(()=>{
    const getFavCars = JSON.parse(localStorage.getItem("fav"));
    const {id} = props;
    if(getFavCars){
      getFavCars.forEach((favId)=>{
      if(favId === id){
        setStarColor('yellow');
      }
      })
    }
  },[])

  const openBasket = () => {
    setShowBasket(true);
  };

  const closeBasket = () => {
    setShowBasket(false);
  };
  const handleAddtoCart = (props) => {
    openBasket();
    getId();
  };

  const drawStar = (props) => {
    if (starColor === "white") {
      setStarColor("yellow");
    } else if (starColor === "yellow") {
      setStarColor("white");
    }

    addToFavourites();
  };

  const {
    name,
    color,
    url,
    price,
    addToPurchased,
    getId,
    addToFavourites,
    showBtn,
    removeFromPurchase,
  } = props;
  const modalBasket = (
    <Modal
      text={
        <p>
          Confirm your purchase :<br />
          <img src={url} alt="" width="200" height="150" />
          <br /> {name}
          <br />
          color: {color}. Price: {price}$
        </p>
      }
      header={"Confirmation"}
      action={closeBasket}
      actions={
        <>
          <Button
            className={"btn"}
            text={"ok"}
            background={"orange"}
            action={() => {
              addToPurchased();
            }}
          />
          <Button
            className={"btn"}
            text={"cancel"}
            action={closeBasket}
            background={"orange"}
          />
        </>
      }
    />
  );
  return (
    <>
      {showBasket && modalBasket}
      <div>
        <div className="card-container">
          {props.closeBtn && (
            <Button
              className="close-btn"
              text={"X"}
              background={"red"}
              action={() => {
                removeFromPurchase();
              }}
            />
          )}
          <img src={url} alt="" width="200" height="150" />
          <p>
            {name}
            <BsFillStarFill color={starColor} onClick={drawStar} />
          </p>
          <p>Color:{color}</p>
          <p className="price">Price:{price}$</p>
          {props.showBtn && (
            <Button
              className={"btn"}
              text={"Add to card"}
              background={"orange"}
              action={handleAddtoCart}
            />
          )}
        </div>
      </div>
    </>
  );
};

export default Card;
