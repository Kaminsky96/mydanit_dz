import React, { useState, useEffect } from "react";
import "../../App.scss";
import Card from "../Card/Card";

// class CardList extends Component {
//     constructor(props){
//         super(props);
//         this.state = {
//             purchased : null,
//             showBasket:false ,
//         }
//     }

//     // openBasket = () => {
//     //         this.setState({ showBasket: true });

//     //     };

//     getId =(id)=> {
//         this.setState({purchased:id})
//     }

//     addToPurchased = () => {
//         const cart = JSON.parse(localStorage.getItem('purchased'));
//         if(cart){
//             const indexCart = cart.indexOf(this.state.purchased)
//             if(indexCart === -1){
//                 localStorage.setItem('purchased',JSON.stringify([...cart,this.state.purchased]))
//             }
//         }
//         else {
//             localStorage.setItem('purchased', JSON.stringify([this.state.purchased]));
//         }

//         this.setState({ showBasket: false });
//     }

//     addToFavourites = (id)=>{

//             const fav = localStorage.getItem('fav')

//             if(fav) {

//                 const parsedArray = JSON.parse(fav)

//                 const index = parsedArray.indexOf(id)

//                 if(index === -1) {
//                     localStorage.setItem('fav', JSON.stringify([...parsedArray, id]))
//                 } else {
//                     parsedArray.splice(index, 1)

//                     localStorage.setItem('fav', JSON.stringify(parsedArray))

//                     if(parsedArray.length === 0) {
//                         localStorage.removeItem('fav')
//                     }
//                 }

//             } else {
//                 localStorage.setItem('fav', JSON.stringify([id]))
//             }

//         }
//         render(){
//            const {items, name, color, price, id} = this.props;

//             return (
//                 <>

//                 <div className="cardlist-container">
//                     {items.map (item=>(
//                     <Card key={item.id}
//                     name={item.name}
//                     color={item.color} starColor={this.state.starColor}  price={item.price} url={item.url} addToFavourites={() => this.addToFavourites(item.id)}
//                     addToPurchased={this.addToPurchased}
//                     getId={()=>this.getId(item.id)}
//                     />
//                     ))}
//                 </div>
//                 </>

//             )
//         }
// }
// export default CardList;

const CardList = (props) => {
  const [purchased, setPurchased] = useState(null);
  const [showBasket, setShowBasket] = useState(false);
  const [isShown, setIsShown] = useState(true);

  const getId = (id) => {
    setPurchased(id);
  };

  const addToPurchased = () => {
    const cart = JSON.parse(localStorage.getItem("purchased"));
    if (cart) {
      const indexCart = cart.indexOf(purchased);
      if (indexCart === -1) {
        localStorage.setItem("purchased", JSON.stringify([...cart, purchased]));
      }
    } else {
      localStorage.setItem("purchased", JSON.stringify([purchased]));
    }
    
    setShowBasket(false);
  };

  

  const addToFavourites = (id) => {
    const fav = localStorage.getItem("fav");

    if (fav) {
      const parsedArray = JSON.parse(fav);

      const index = parsedArray.indexOf(id);

      if (index === -1) {
        localStorage.setItem("fav", JSON.stringify([...parsedArray, id]));
      } else {
        parsedArray.splice(index, 1);

        localStorage.setItem("fav", JSON.stringify(parsedArray));

        if (parsedArray.length === 0) {
          localStorage.removeItem("fav");
        }
      }
    } else {
      localStorage.setItem("fav", JSON.stringify([id]));
    }
  };

  const { items, name, color, price, id, starColor, showBtn, closeBtn,removeFromPurchase} = props;

  return (
    <>
      <div className="cardlist-container">
        {items.map((item) => (
          <Card
            id={item.id}
            key={item.id}
            name={item.name}
            color={item.color}
            starColor={starColor}
            price={item.price}
            url={item.url}
            addToFavourites={() => addToFavourites(item.id)}
            addToPurchased={addToPurchased}
            getId={() => getId(item.id)}
            showBtn={showBtn}
            closeBtn={closeBtn}
            removeFromPurchase={()=> removeFromPurchase(item.id)}
          />
        ))}
      </div>
    </>
  );
};
export default CardList;
