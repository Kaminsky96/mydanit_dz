import React ,{ Component } from "react";
import '../../App.scss'
import Button from '../Button/Button.jsx';


// class Modal extends Component {
    
//        render(){
        
//         return (
//             <div className='modal-container' onClick={this.props.action}>
//             <div className="modal">
//                 <div className="modal-header">
//                     {this.props.header}
//                     {this.props.closeBtn &&  <Button text={'x'}  background={'firebrick'} action={this.props.action}/>}
//                 </div>
//                 {this.props.text}
//                 {this.props.actions}
//             </div>
//             </div>
//         )
//     }
// }

// export default Modal;

const Modal =(props) => {
        
        return (
            <div className='modal-container' onClick={props.action}>
            <div className="modal">
                <div className="modal-header">
                    {props.header}
                    {props.closeBtn &&  <Button text={'x'}  background={'firebrick'} action={props.action}/>}
                </div>
                {props.text}
                {props.actions}
            </div>
            </div>
        )
    }


export default Modal;