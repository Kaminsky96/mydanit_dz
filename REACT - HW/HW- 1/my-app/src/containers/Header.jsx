import React from 'react';
import { Link } from 'react-router-dom';

const Header = () => {
    return (
        <header className="header-container">
                <ul className="header-menu">
                    <Link to="/Home" className="header-menu-item">Home</Link>
                    <Link to="/Bascet" className="header-menu-item">Bascet</Link>
                    <Link to="/Favourites" className="header-menu-item">Favourites</Link>
                </ul>
        </header>
    )
}
export default Header;