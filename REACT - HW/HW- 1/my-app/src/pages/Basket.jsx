import React, { useState, useEffect } from "react";
import CardList from "../components/CarsList/CardList";
import getCars from "../api/getCars";
import Card from "../components/Card/Card.jsx";

const Basket = (props) => {
  const [cars, setCars] = useState([]);
  const [noPurchase,setNoPurchase] = useState(true);
  const getPurchasedCars = JSON.parse(localStorage.getItem("purchased"));
  useEffect(() => {
    getCars().then((cars) => setCars(cars));
    if(getPurchasedCars){
        setNoPurchase(false)
    }
  });

  const purchasedCars = cars.filter((item) => {
    if (getPurchasedCars === null) {
      return false;
    }
    if (getPurchasedCars.includes(item.id)) {
      return item;
    }
  });
 

  const removeFromPurchase = (id) => {
      const cart = JSON.parse(localStorage.getItem("purchased"));
      const index = cart.indexOf(id);
      cart.splice(index, 1);

      localStorage.setItem("purchased", JSON.stringify(cart));
    if (cart.length === 0) {
      localStorage.removeItem("purchased");
      setNoPurchase(true);
    }
  };


  return (
    <>
     {noPurchase && <p className="basket-purchase">No items</p>} 
      <CardList items={purchasedCars} closeBtn removeFromPurchase={removeFromPurchase}/>
    </>
  );
};

export default Basket;
