import React, { useState, useEffect } from "react";
import CardList from "../components/CarsList/CardList";
import getCars from "../api/getCars";

const Favourites = (props) => {
  const getFavCars = JSON.parse(localStorage.getItem("fav"));
  const [cars, setCars] = useState([]);
  const [noFav,setNoFav] = useState(true);
  useEffect(() => {
    getCars().then((cars) => setCars(cars));
    if(getFavCars){
        setNoFav(false)
    }
  });

  
  const favouriteCars = cars.filter((item) => {
    if (getFavCars === null) {
      return false;
    }
    if (getFavCars.includes(item.id)) {
      return item;
    }
  });

  return (
    <>
      { noFav && <p className="basket-purchase">No items</p>}
      <CardList items={favouriteCars} />
    </>
  );
};

export default Favourites;
