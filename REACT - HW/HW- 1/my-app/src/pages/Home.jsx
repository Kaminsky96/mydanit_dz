import React, { useState, useEffect } from "react";
import CardList from "../components/CarsList/CardList";
import getCars from "../api/getCars.js";
// class Home extends Component {
//     state = {
//         cars:[]
//       };
//       componentDidMount(){
//         getCars().then(cars => this.setState({cars}))
//       }

//           render(){
// return(

// <CardList items={this.state.cars}/>

// )
// }

// }

// export default Home;

const Home = (props) => {
  const [cars, setCars] = useState([]);

  useEffect(() => {
    getCars().then((cars) => setCars(cars));
  }, []);

  return <CardList items={cars} showBtn />;
};

export default Home;
