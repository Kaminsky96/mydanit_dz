// ------------------------ TASK 1----------------------------------------------
const clients1 = ["Гилберт", "Сальваторе", "Пирс", "Соммерс", "Форбс", "Донован", "Беннет"];
const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];


const newClients = Array.from(new Set(clients1.concat(clients2)));
console.log(newClients);

// //-------------------------------TASK 2 ----------------------------------------
const characters = [
    {
      name: "Елена",
      lastName: "Гилберт",
      age: 17, 
      gender: "woman",
      status: "human"
    },
    {
      name: "Кэролайн",
      lastName: "Форбс",
      age: 17,
      gender: "woman",
      status: "human"
    },
    {
      name: "Аларик",
      lastName: "Зальцман",
      age: 31,
      gender: "man",
      status: "human"
    },
    {
      name: "Дэймон",
      lastName: "Сальваторе",
      age: 156,
      gender: "man",
      status: "vampire"
    },
    {
      name: "Ребекка",
      lastName: "Майклсон",
      age: 1089,
      gender: "woman",
      status: "vempire"
    },
    {
      name: "Клаус",
      lastName: "Майклсон",
      age: 1093,
      gender: "man",
      status: "vampire"
    }
  ];

  
  

const charactersShortInfo = characters.map(function(item){
const {name, lastName, age} = item;
    return {
        name,
        lastName,
        age
}

})

console.log(charactersShortInfo);

// function x (...rest){
//   console.log(rest);
// }
// x(1, 'ff');



// ------------------------------------------- task3 ------------------------------------------------

const user1 = {
    name: "John",
    years: 30
  };

  const {
      name:name,
      years:age,
      isadmin:isAdmin = false,
  } = user1;

  console.log(isAdmin);
  console.log(age);
  console.log(name);
//---------------------------------------------task 4--------------------------------------------------



  
const satoshi2020 = {
    name: 'Nick',
    surname: 'Sabo',
    age: 51,
    country: 'Japan',
    birth: '1979-08-21',
    location: {
      lat: 38.869422, 
      lng: 139.876632
    }
  }
  const satoshi2019 = {
    name: 'Dorian',
    surname: 'Nakamoto',
    age: 44,
    hidden: true,
    country: 'USA',
    wallet: '1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa',
    browser: 'Chrome'
  }

  const satoshi2018 = {
    name: 'Satoshi',
    surname: 'Nakamoto', 
    technology: 'Bitcoin',
    country: 'Japan',
    browser: 'Tor',
    birth: '1975-04-05'
  }

  
  const fullProfile =  Object.assign(satoshi2018,satoshi2019,satoshi2020);
  

  console.log(fullProfile);

//   -------------------------------------task 5---------------------------------
  const books = [{
    name: 'Harry Potter',
    author: 'J.K. Rowling'
  }, {
    name: 'Lord of the rings',
    author: 'J.R.R. Tolkien'
  }, {
    name: 'The witcher',
    author: 'Andrzej Sapkowski'
  }];
  
  const bookToAdd = {
    name: 'Game of thrones',
    author: 'George R. R. Martin'
  }

  const newBookList = books.concat(bookToAdd);
  console.log(newBookList);
  console.log(books);

//   const newBookList = [...books,bookToAdd];
//   console.log(newBookList);
//   console.log(books);

//------------------------------------------ task 6 -------------------------------

// здесь я совсем не уверен , немного не понял как просто 
// добавить свойства и при этом вывести новый обьект
// во втором варианте они добавились в прото 
const employee = {
    name: 'Vitalii',
    surname: 'Klichko'
  }
  
  const addData = {
      age: 45,
      salary: 1000
  }

  const newEmployee = Object.assign({},employee,addData);
  console.log(newEmployee);
  console.log(employee);


// const newEmployee = Object.create(employee);
// newEmployee.age = 45;
// newEmployee.salary = 1000;
// console.log(newEmployee);


//--------------------------------------------- task 7 ----------------------------------------

const array = ['value', () => 'showValue'];

// Допишите ваш код здесь
const [ value = 'value', showValue = () =>{return 'showValue'}] = array;



alert(value); // должно быть выведено 'value'
alert(showValue());  // должно быть выведено 'showValue'