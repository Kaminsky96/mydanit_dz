const books = [
  {
    author: "Скотт Бэккер",
    name: "Тьма, что приходит прежде",
    price: 70,
  },
  {
    author: "Скотт Бэккер",
    name: "Воин-пророк",
  },
  {
    name: "Тысячекратная мысль",
    price: 70,
  },
  {
    author: "Скотт Бэккер",
    name: "Нечестивый Консульт",
    price: 70,
  },
  {
    author: "Дарья Донцова",
    name: "Детектив на диете",
    price: 40,
  },
  {
    author: "Дарья Донцова",
    name: "Дед Снегур и Морозочка",
  },
];

const div = document.createElement("div");
document.body.prepend(div);
div.id = "root";
const ul = document.createElement("ul");
div.append(ul);

books.forEach(function validateBook(book) {
  try {
    if (!book.hasOwnProperty("author")) {
      throw new Error(`author is missing at ${book.name}`);
    } else if (!book.hasOwnProperty("price")) {
      throw new Error(`price is missing at ${book.name}`);
    } else
      book.hasOwnProperty("name") &&
        book.hasOwnProperty("price") &&
        book.hasOwnProperty("author");
    const li = document.createElement("li");
    ul.append(li);
    li.insertAdjacentHTML("afterbegin", book.name);
  } catch (Error) {
    console.log("from catch-->");
    console.log(Error);
  }
});
