class Employee {
  constructor(name, age, salary) {
    this.name = name;
    this.age = age;
    this.salary = salary;
  }
}

class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this.lang = lang;
    this.salary = salary * 3;
  }
}

const ira = new Employee("Ira", 28, 1500);
const yura = new Programmer("Yura", 24, 1000, "Javascript");
const valera = new Programmer("Valera", 22, 2000, "Java");

console.log(ira);
console.log(yura);
console.log(valera);
