const div = document.createElement("div");
document.body.prepend(div);
const ul = document.createElement("ul");
div.append(ul);

const renderFilm = function (data) {
  const { episode_id, opening_crawl, title } = data.results;
  const allData = data.results;
  allData.forEach((element) => {
    const li = document.createElement("li");
    li.classList.add("film");
    ul.append(li);
    li.insertAdjacentHTML(
      "afterbegin",
      `<p>Episode:${element.episode_id}</p>
    <p>Title:${element.title}</p>
    <p>Description:${element.opening_crawl}
    <p>CHARACTERS</p>`
    );
    element.characters.forEach((character) => {
      fetch(`${character}`)
        .then((response) => response.json())
        .then((data) => {
          li.insertAdjacentHTML("beforeend", `<li>${data.name}</li>`);
        });
    });
  });
};

fetch("https://swapi.dev/api/films/")
  .then((response) => response.json())
  .then((data) => {
    renderFilm(data);
  });
