const ipButton = document.querySelector(".ip-btn");
async function makeRequest() {
  const response = await fetch("https://api.ipify.org/?format=json");
  const data = await response.json();
  const nextResponse = await fetch(`http://ip-api.com/json/${data.ip}`);
  const nextResponseData = await nextResponse.json();
  console.log(nextResponseData);
  renderIP(nextResponseData);
}

const renderIP = function (data) {
  const div = document.createElement("div");
  const { city, country, countryCode, region, timezone } = data;
  div.insertAdjacentHTML(
    "afterbegin",
    `<p>city:${city}</p>
   <p>country:${country}</p>
   <p>code:${countryCode}</p>
   <p>region:${region}</p>
   <p>timezone:${timezone}</p>`
  );
  ipButton.after(div);
};

ipButton.addEventListener("click", (event) => {
  makeRequest();
});
