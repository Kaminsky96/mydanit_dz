// табы в секции our services
const tabs = document.getElementsByClassName('tab');  
const sections = document.getElementsByClassName('tab-content'); 

[...tabs].forEach(tab => tab.addEventListener('click', tabClick));

function tabClick(event) {
  const tabId = event.target.id;

  [...tabs].forEach((tab, i) => {
    tab.classList.remove('active');
    sections[i].classList.remove('content-active');
  })

  tabs[tabId - 1].classList.add('active');
  sections[tabId - 1].classList.add('content-active');
}
// фильтр в секции Our amazing work 
const container = document.querySelector('.amazing-images-active');
const hover = {
    graph:`<div class="test-img">
    <a href="" class="first-link"><svg width="15" height="15" viewBox="0 0 15 15" fill="none"
        xmlns="http://www.w3.org/2000/svg">
        <path fill-rule="evenodd" clip-rule="evenodd"
          d="M13.9131 2.72817L12.0948 0.891285C11.2902 0.0808612 9.98305 0.0759142 9.17681 0.882615L7.15921 2.89256C6.35161 3.69885 6.34818 5.01032 7.15051 5.82074L8.30352 4.68897C8.18678 4.32836 8.33041 3.9153 8.61593 3.62946L9.89949 2.35187C10.3061 1.94624 10.9584 1.94913 11.3595 2.35434L12.4513 3.45805C12.8528 3.86283 12.8511 4.51713 12.447 4.92318L11.1634 6.20241C10.8918 6.47296 10.4461 6.62168 10.1002 6.52626L8.97094 7.65887C9.77453 8.47177 11.0803 8.47466 11.8889 7.66837L13.9039 5.65924C14.7141 4.85254 14.7167 3.53983 13.9131 2.72817ZM6.52613 10.0918C6.62191 10.4441 6.46857 10.8997 6.19093 11.1777L4.99227 12.3752C4.58074 12.7845 3.91595 12.7833 3.50671 12.369L2.39297 11.2475C1.98465 10.8349 1.98729 10.1633 2.39824 9.75473L3.59804 8.55769C3.89032 8.26607 4.31044 8.12025 4.67711 8.24375L5.83354 7.0715C5.01493 6.2462 3.68249 6.24207 2.86059 7.06324L0.915197 9.0042C0.0922615 9.8266 0.0883685 11.1629 0.90651 11.9886L2.75817 13.8618C3.57595 14.6846 4.90724 14.6912 5.73111 13.8701L7.67649 11.9287C8.49852 11.1054 8.5024 9.77166 7.68553 8.9443L6.52613 10.0918ZM6.25787 9.56307C5.98013 9.84189 5.53427 9.84105 5.26179 9.55812C4.98792 9.27434 4.9901 8.82039 5.26613 8.53993L8.59075 5.16109C8.86679 4.88227 9.31174 4.88311 9.58513 5.16398C9.86048 5.44569 9.85876 5.90088 9.5817 6.18299L6.25787 9.56307Z"
          fill="#1FDAB5" />
      </svg></a>
    <a href="" class="second-link"><svg width="12" height="11" viewBox="0 0 12 11" fill="none"
        xmlns="http://www.w3.org/2000/svg">
        <rect width="12" height="11" fill="white" />
      </svg></a>
    <p class="creative-design-text">Awesome design</p>
    <p class="category-name">Graphic design</p>
  </div>`,
  web:`<div class="test-img">
  <a href="" class="first-link"><svg width="15" height="15" viewBox="0 0 15 15" fill="none"
      xmlns="http://www.w3.org/2000/svg">
      <path fill-rule="evenodd" clip-rule="evenodd"
        d="M13.9131 2.72817L12.0948 0.891285C11.2902 0.0808612 9.98305 0.0759142 9.17681 0.882615L7.15921 2.89256C6.35161 3.69885 6.34818 5.01032 7.15051 5.82074L8.30352 4.68897C8.18678 4.32836 8.33041 3.9153 8.61593 3.62946L9.89949 2.35187C10.3061 1.94624 10.9584 1.94913 11.3595 2.35434L12.4513 3.45805C12.8528 3.86283 12.8511 4.51713 12.447 4.92318L11.1634 6.20241C10.8918 6.47296 10.4461 6.62168 10.1002 6.52626L8.97094 7.65887C9.77453 8.47177 11.0803 8.47466 11.8889 7.66837L13.9039 5.65924C14.7141 4.85254 14.7167 3.53983 13.9131 2.72817ZM6.52613 10.0918C6.62191 10.4441 6.46857 10.8997 6.19093 11.1777L4.99227 12.3752C4.58074 12.7845 3.91595 12.7833 3.50671 12.369L2.39297 11.2475C1.98465 10.8349 1.98729 10.1633 2.39824 9.75473L3.59804 8.55769C3.89032 8.26607 4.31044 8.12025 4.67711 8.24375L5.83354 7.0715C5.01493 6.2462 3.68249 6.24207 2.86059 7.06324L0.915197 9.0042C0.0922615 9.8266 0.0883685 11.1629 0.90651 11.9886L2.75817 13.8618C3.57595 14.6846 4.90724 14.6912 5.73111 13.8701L7.67649 11.9287C8.49852 11.1054 8.5024 9.77166 7.68553 8.9443L6.52613 10.0918ZM6.25787 9.56307C5.98013 9.84189 5.53427 9.84105 5.26179 9.55812C4.98792 9.27434 4.9901 8.82039 5.26613 8.53993L8.59075 5.16109C8.86679 4.88227 9.31174 4.88311 9.58513 5.16398C9.86048 5.44569 9.85876 5.90088 9.5817 6.18299L6.25787 9.56307Z"
        fill="#1FDAB5" />
    </svg></a>
  <a href="" class="second-link"><svg width="12" height="11" viewBox="0 0 12 11" fill="none"
      xmlns="http://www.w3.org/2000/svg">
      <rect width="12" height="11" fill="white" />
    </svg></a>
  <p class="creative-design-text">Creative design</p>
  <p class="category-name">Web design</p>
</div>`,
wordpress:`<div class="test-img">
<a href="" class="first-link"><svg width="15" height="15" viewBox="0 0 15 15" fill="none"
    xmlns="http://www.w3.org/2000/svg">
    <path fill-rule="evenodd" clip-rule="evenodd"
      d="M13.9131 2.72817L12.0948 0.891285C11.2902 0.0808612 9.98305 0.0759142 9.17681 0.882615L7.15921 2.89256C6.35161 3.69885 6.34818 5.01032 7.15051 5.82074L8.30352 4.68897C8.18678 4.32836 8.33041 3.9153 8.61593 3.62946L9.89949 2.35187C10.3061 1.94624 10.9584 1.94913 11.3595 2.35434L12.4513 3.45805C12.8528 3.86283 12.8511 4.51713 12.447 4.92318L11.1634 6.20241C10.8918 6.47296 10.4461 6.62168 10.1002 6.52626L8.97094 7.65887C9.77453 8.47177 11.0803 8.47466 11.8889 7.66837L13.9039 5.65924C14.7141 4.85254 14.7167 3.53983 13.9131 2.72817ZM6.52613 10.0918C6.62191 10.4441 6.46857 10.8997 6.19093 11.1777L4.99227 12.3752C4.58074 12.7845 3.91595 12.7833 3.50671 12.369L2.39297 11.2475C1.98465 10.8349 1.98729 10.1633 2.39824 9.75473L3.59804 8.55769C3.89032 8.26607 4.31044 8.12025 4.67711 8.24375L5.83354 7.0715C5.01493 6.2462 3.68249 6.24207 2.86059 7.06324L0.915197 9.0042C0.0922615 9.8266 0.0883685 11.1629 0.90651 11.9886L2.75817 13.8618C3.57595 14.6846 4.90724 14.6912 5.73111 13.8701L7.67649 11.9287C8.49852 11.1054 8.5024 9.77166 7.68553 8.9443L6.52613 10.0918ZM6.25787 9.56307C5.98013 9.84189 5.53427 9.84105 5.26179 9.55812C4.98792 9.27434 4.9901 8.82039 5.26613 8.53993L8.59075 5.16109C8.86679 4.88227 9.31174 4.88311 9.58513 5.16398C9.86048 5.44569 9.85876 5.90088 9.5817 6.18299L6.25787 9.56307Z"
      fill="#1FDAB5" />
  </svg></a>
<a href="" class="second-link"><svg width="12" height="11" viewBox="0 0 12 11" fill="none"
    xmlns="http://www.w3.org/2000/svg">
    <rect width="12" height="11" fill="white" />
  </svg></a>
<p class="creative-design-text">Functional</p>
<p class="category-name">Wordpress</p>
</div>`,
landing:`<div class="test-img">
<a href="" class="first-link"><svg width="15" height="15" viewBox="0 0 15 15" fill="none"
    xmlns="http://www.w3.org/2000/svg">
    <path fill-rule="evenodd" clip-rule="evenodd"
      d="M13.9131 2.72817L12.0948 0.891285C11.2902 0.0808612 9.98305 0.0759142 9.17681 0.882615L7.15921 2.89256C6.35161 3.69885 6.34818 5.01032 7.15051 5.82074L8.30352 4.68897C8.18678 4.32836 8.33041 3.9153 8.61593 3.62946L9.89949 2.35187C10.3061 1.94624 10.9584 1.94913 11.3595 2.35434L12.4513 3.45805C12.8528 3.86283 12.8511 4.51713 12.447 4.92318L11.1634 6.20241C10.8918 6.47296 10.4461 6.62168 10.1002 6.52626L8.97094 7.65887C9.77453 8.47177 11.0803 8.47466 11.8889 7.66837L13.9039 5.65924C14.7141 4.85254 14.7167 3.53983 13.9131 2.72817ZM6.52613 10.0918C6.62191 10.4441 6.46857 10.8997 6.19093 11.1777L4.99227 12.3752C4.58074 12.7845 3.91595 12.7833 3.50671 12.369L2.39297 11.2475C1.98465 10.8349 1.98729 10.1633 2.39824 9.75473L3.59804 8.55769C3.89032 8.26607 4.31044 8.12025 4.67711 8.24375L5.83354 7.0715C5.01493 6.2462 3.68249 6.24207 2.86059 7.06324L0.915197 9.0042C0.0922615 9.8266 0.0883685 11.1629 0.90651 11.9886L2.75817 13.8618C3.57595 14.6846 4.90724 14.6912 5.73111 13.8701L7.67649 11.9287C8.49852 11.1054 8.5024 9.77166 7.68553 8.9443L6.52613 10.0918ZM6.25787 9.56307C5.98013 9.84189 5.53427 9.84105 5.26179 9.55812C4.98792 9.27434 4.9901 8.82039 5.26613 8.53993L8.59075 5.16109C8.86679 4.88227 9.31174 4.88311 9.58513 5.16398C9.86048 5.44569 9.85876 5.90088 9.5817 6.18299L6.25787 9.56307Z"
      fill="#1FDAB5" />
  </svg></a>
<a href="" class="second-link"><svg width="12" height="11" viewBox="0 0 12 11" fill="none"
    xmlns="http://www.w3.org/2000/svg">
    <rect width="12" height="11" fill="white" />
  </svg></a>
<p class="creative-design-text">Variaty of patterns </p>
<p class="category-name">Landing page</p>
</div>`
  }

  for (let i = 1; i <=3; i++) {
      container.insertAdjacentHTML('beforeend', `<div class="img-content" data-id="graphic"><img src="./images/graphic-design/graphic-design${i}.jpg" alt="">${hover.graph}</div>`)
      container.insertAdjacentHTML('beforeend', `<div class="img-content" data-id="web"><img src="./images/web-design/web-design${i}.jpg" alt="">${hover.web}</div>`)
      container.insertAdjacentHTML('beforeend', `<div class="img-content" data-id="landing"><img src="./images/landing-page/landing-page${i}.jpg" alt="">${hover.landing}</div>`)
      container.insertAdjacentHTML('beforeend', `<div class="img-content" data-id="wordpress"><img src="./images/wordpress/wordpress${i}.jpg" alt="">${hover.wordpress}</div>`)

      
  }
  const btn = document.querySelector('.load-more-btn');

  btn.addEventListener('click',()=> {
    for (let i = 4; i <=6; i++) {
        container.insertAdjacentHTML('beforeend', `<div class="img-content" data-id="graphic"><img src="./images/graphic-design/graphic-design${i}.jpg" alt="">${hover.graph}</div>`)
        container.insertAdjacentHTML('beforeend', `<div class="img-content" data-id="web"><img src="./images/web-design/web-design${i}.jpg" alt="">${hover.web}</div>`)
        container.insertAdjacentHTML('beforeend', `<div class="img-content" data-id="landing"><img src="./images/landing-page/landing-page${i}.jpg" alt="">${hover.landing}</div>`)
        container.insertAdjacentHTML('beforeend', `<div class="img-content" data-id="wordpress"><img src="./images/wordpress/wordpress${i}.jpg" alt="">${hover.wordpress}</div>`)
  
        
    }
    btn.remove();
      

  })

  const tabsTitle = document.querySelectorAll('.amazing-work-tab');
  tabsTitle.forEach(item=> {
      item.addEventListener('click',(e)=>{
          const allImg = document.querySelectorAll('.img-content');
        tabsTitle.forEach(currentItem => {
            currentItem.classList.remove('work-tab-active')
              
          });
          e.target.classList.add('work-tab-active')
          allImg.forEach(img=>{
              if(e.target.dataset.title === img.dataset.id){
                  img.style.display = 'block';
              }
              else if (e.target.dataset.title === 'all'){
                img.style.display = 'block';
              }
              else{
                  img.style.display = 'none';
              }
          })
      })

  })

// /// SLIDER
const img = document.querySelectorAll('.people-comment');
const prev = document.querySelector('.btn-left');
const next = document.querySelector('.btn-right');
const tabsSlider = document.querySelector('.tabsSlider');
const tabsItem = document.querySelectorAll('.people-icon');
let counter = 0;
next.addEventListener('click', () => {
	img[counter].classList.remove('active-comment');
	tabsItem[counter].classList.remove('active-icon');
	counter = (counter + 1) % img.length;
	tabsItem[counter].classList.add('active-icon');
    img[counter].classList.add('active-comment');
    event.preventDefault();
});
prev.addEventListener('click', () => {
	img[counter].classList.remove('active-comment');
	tabsItem[counter].classList.remove('active-icon');
	if (counter === 0) {
		counter = img.length;
	}
	counter = (counter - 1) % img.length;
	tabsItem[counter].classList.add('active-icon');
    img[counter].classList.add('active-comment');
    event.preventDefault();
});
tabsSlider.addEventListener('click', event => {
	if (event.target.classList.contains('tabsSlider')) {
		return;
	}
	tabsItem.forEach(currentItem => {
		currentItem.classList.remove('active-icon');
	});
	// img.forEach((item, index) => {
	// 	if (event.target === item) {
	// 		item.classList.add('active-comment');
	// 		counter = index;
	// 	} else {
	// 		item.classList.remove('active-comment');
	// 	}
	// });
	event.target.classList.add('active-icon');
});



